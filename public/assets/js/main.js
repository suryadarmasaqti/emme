$(document).ready(function(){

    //EditURL
    $('.url-edit').click(function(){
        $(this).removeClass('active').addClass('nonactive');
        $('.url-save').removeClass('nonactive').addClass('active');
        $('.in').removeClass('nonactive').addClass('active');
        $('.tx').removeClass('active').addClass('nonactive');

        var url = $('.in input');
        var urlLength = url.val().length * 2;
        url.focus();
        url[0].setSelectionRange(urlLength, urlLength);
    });
    $('.url-save').click(function(){
        $(this).removeClass('active').addClass('nonactive');
        $('.url-edit').removeClass('nonactive').addClass('active');
        $('.tx').removeClass('nonactive').addClass('active');
        $('.in').removeClass('active').addClass('nonactive');
    });

    //b_left
    bleft();
    function bleft(){
        var side = $(".b_left"),
            nBox = $("#n_bx-out").outerHeight();

        side.css({
            'height' : nBox + 70
        })
    }

    //Menu
    menu();

    function menu(){

        var iconMenu        = $('.n_nav-xs');

        iconMenu.click(function(e){
            e.stopPropagation();
            var nav_xs   = $('.n_nav');
            nav_xs.toggleClass('open');
        });

        //$('.nav ul').on("click", function (e) {
        //    e.stopPropagation();
        //});

        $(document).click(function() {
            $('.n_nav').removeClass('open');
        });
    }

    //Wrapper
    wrapper();

    function wrapper(){
        var windheight = $(window).height();
        var footheight = $('.footer').outerHeight();
        var wrap = $('.wrapper');

        wrap.css({
            'min-height': windheight,
            'padding-bottom': footheight,
            'position': 'relative',
        })
    }

    $(window).resize(function(){
        wrapper();
    });

    // CKEditor
    //$('#ckeditor').ckeditor();

    //$('#inlineedit1, #inlineedit2').ckeditor();

    // Uncomment the following code to test the "Timeout Loading Method".
    // CKEDITOR.loadFullCoreTimeout = 5;

    window.onload = function() {
        // Listen to the double click event.
        if ( window.addEventListener )
            document.body.addEventListener( 'dblclick', onDoubleClick, false );
        else if ( window.attachEvent )
            document.body.attachEvent( 'ondblclick', onDoubleClick );
    };

    function onDoubleClick( ev ) {
        // Get the element which fired the event. This is not necessarily the
        // element to which the event has been attached.
        var element = ev.target || ev.srcElement;

        // Find out the div that holds this element.
        var name;

        do {
            element = element.parentNode;
        }
        while ( element && ( name = element.nodeName.toLowerCase() ) &&
        ( name != 'div' || element.className.indexOf( 'editable' ) == -1 ) && name != 'body' );

        if ( name == 'div' && element.className.indexOf( 'editable' ) != -1 )
            replaceDiv( element );
    }

    var editor;

    function replaceDiv( div ) {
        if ( editor )
            editor.destroy();
        editor = CKEDITOR.replace( div );
    }

});
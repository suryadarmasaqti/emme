<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBank extends Migration
{
    public function up()
    {
     
        Schema::create('bank', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('code');
            $table->String('name');
            $table->string('headquarter');
            $table->timestamps();
            $table->softDeletes();
        });
    }
        
    public function down()
    {
        Schema::drop('bank');
    }
}

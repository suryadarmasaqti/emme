<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Database\Eloquent\SoftDeletes;
class Bank extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

	protected $guarded  = array('id');

	protected $fillable = array('name','code','headquarter' );

	/**
	 * The rules for email field, automatic validation.
	 *
	 * @var array
	*/
	protected $table = 'bank';
	private $rules = array(
			'code' => 'required|min:2',
			'name' => 'required|min:2',
			'headquarter' => 'required|min:2'
	);
	
}

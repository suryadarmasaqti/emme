<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Input;
use App\Bank;
use App\Http\Requests\Admin\BankRequest;
use App\Http\Requests\Admin\DeleteRequest;
use App\Http\Requests\Admin\ReorderRequest;
use Illuminate\Support\Facades\Auth;
use Datatables;
use App\Http\Controllers\Admin\Controller;
use Redirect;

class BankController extends AdminController
{
    public function __construct()
    {
        view()->share('type', 'bank');
    }

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        // Show the page
    $bank= Bank::paginate(10);
        return view('admin.bank.index',compact('bank'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.bank.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(BankRequest $request)
    {  
        $language = new Bank($request->all());
        $language -> save();
        return Redirect::to('admin/bank')->with('message', '*Bank successfully Added..');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param bank
     * @return Response
     */
    public function edit($bank)
    {
        $bank=Bank::where('id',$bank)->first();
        return view('admin.bank.create_edit', compact('bank'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param bank
     * @return Response
     */
    public function update(BankRequest $request, $bank)
    {
       $bank=Bank::where('id',$bank)->first();
       $bank -> update($request->all());
       return Redirect::to('admin/bank')->with('message', '*Bank successfully Edited..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param bank
     * @return Response
     */

    public function delete($bank)
    {
        $bank=Bank::where('id',$bank)->first();
        return view('admin.bank.delete', compact('bank'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param bank
     * @return Response
     */
    public function destroy($bank)
    {
       $bank=Bank::where('id',$bank)->first();
       $bank->delete();
       return Redirect::to('admin/bank')->with('message', '*Bank successfully deleted..');
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolesModel extends Model
{
    protected $primarykey = 'id';
    protected $table = 'roles';
    protected $fillable = array('roles_name','roles_access' );
    public $timestamps = false;
}

@extends('admin.layouts.default')
{{-- Content --}}
@section('main')

<div class="n_titlepage ptd20">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <h2 class="str s21 mb0">Add New User</h2>
            </div>
            <div class="col-md-3 col-sm-4"></div>
        </div>
    </div>
</div>

{!! Form::open(array('url' => URL::to('admin/user'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
<div class="ptd60">
    <div class="container">
        <div class="n_bx ptd0">
            <div class="row no-gutter">
                <div class="col-md-9 col-sm-8">
                    <div class="p30">
                   
                            <div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
                            {!! Form::label('name', trans("admin/users.name"), array('class' => 'control-label')) !!}
                            <div class="controls">
                            {!! Form::text('name', null, array('class' => 'form-control')) !!}
                            <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                            </div>
                            </div>

                            <div class="form-group  {{ $errors->has('username') ? 'has-error' : '' }}">
                            {!! Form::label('username', trans("admin/users.username"), array('class' => 'control-label')) !!}
                            <div class="controls">
                            {!! Form::text('username', null, array('class' => 'form-control')) !!}
                            <span class="help-block">{{ $errors->first('username', ':message') }}</span>
                            </div>
                            </div>
                            
                            <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
                            {!! Form::label('email', trans("admin/users.email"), array('class' => 'control-label')) !!}
                            <div class="controls">
                            {!! Form::text('email', null, array('class' => 'form-control')) !!}
                            <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                            </div>
                            </div>
                            
                            <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
                            {!! Form::label('password', trans("admin/users.password"), array('class' => 'control-label')) !!}
                            <div class="controls">
                            {!! Form::password('password', array('class' => 'form-control')) !!}
                            <span class="help-block">{{ $errors->first('password', ':message') }}</span>
                            </div>
                            </div>
                            
                            <div class="form-group  {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                            {!! Form::label('password_confirmation', trans("admin/users.password_confirmation"), array('class' => 'control-label')) !!}
                            <div class="controls">
                            {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
                            <span class="help-block">{{ $errors->first('password_confirmation', ':message') }}</span>
                            </div>
                            </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 bg_grey b_left">
                    <div class="p30 b_bottom">
                        <h3 class="str">Publish</h3>
                        <ul class="list-none list-inline tar">
                            <li><a href="../user" class="btn btn-transparent">Cancel</a></li>
                            <li><button type="submit" class="btn btn-blue str">Save</button></li>
                        </ul>
                    </div>
                    <div class="p30">
                        <h3 class="str">Category  Attributes</h3>
                        <form class="form">
                            <div class="form-group  {{ $errors->has('confirmed') ? 'has-error' : '' }}">
                            {!! Form::label('confirmed', trans("admin/users.active_user"), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::label('confirmed', trans("admin/users.yes"), array('class' => 'control-label')) !!}
                                {!! Form::radio('confirmed', '1', @isset($user)? $user->confirmed : 'false') !!}
                                {!! Form::label('confirmed', trans("admin/users.no"), array('class' => 'control-label')) !!}
                                {!! Form::radio('confirmed', '0', @isset($user)? $user->confirmed : 'true') !!}
                                <span class="help-block">{{ $errors->first('confirmed', ':message') }}</span>
                            </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    {!! Form::close() !!}
    @stop @section('scripts')
        <script type="text/javascript">
            $(function () {
                $("#roles").select2()
            });
        </script>
</div>
@stop
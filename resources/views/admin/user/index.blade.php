@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! trans("admin/users.users") !!} :: @parent
@stop

{{-- Content --}}
@section('main')
<form action="all-user-action" method="post" class="form form-inline"><input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
<div class="n_titlepage ptd20">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <h2 class="str s21 mb0">{!! trans("admin/users.users") !!}</h2>
            </div>
            <div class="col-sm-3 tar">
                <div class="pull-right">
                    <a href="{!! URL::to('admin/user/create') !!}"
                       class="btn btn-blue str"><span
                                class="glyphicon glyphicon-plus-sign"></span> {{
                    trans("admin/modal.new") }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ptd60">
<div class="container">
<div class="n_bx ptd0">
<div class="p30">
    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>{!! trans("admin/users.name") !!}</th>
            <th>{!! trans("admin/users.email") !!}</th>
            <th>{!! trans("admin/users.active_user") !!}</th>
            <th>{!! trans("admin/admin.created_at") !!}</th>
            <th>{!! trans("admin/admin.action") !!}</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
@stop

{{-- Scripts --}}
@section('scripts')
@stop

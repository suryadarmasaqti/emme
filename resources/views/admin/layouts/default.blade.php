<!doctype html>
<html>
<head>
    <?php $segment = Request::segment(3); $edit = Request::segment(4); ?>
    <title>@section('title') Administration @show</title>

    @section('meta_keywords')
        <meta name="keywords" content="your, awesome, keywords, here"/>
    @show @section('meta_author')
        <meta name="author" content="Jon Doe"/>
    @show @section('meta_description')
        <meta name="description"
              content="Lorem ipsum dolor sit amet, nihil fabulas et sea, nam posse menandri scripserit no, mei."/>
    @show
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta name="author" content="(c) 2014" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link href="<?php if($edit == "edit"){echo "../";} ?><?php if($segment != ""){echo "../";} ?>../../public/assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="<?php if($edit == "edit"){echo "../";} ?><?php if($segment != ""){echo "../";} ?>../../public/style.css" rel="stylesheet" media="screen">

    <link rel="shortcut icon" href="<?php if($edit == "edit"){echo "../";} ?><?php if($segment != ""){echo "../";} ?>../../public/assets/img/icon/clementi-icn-fav.png" type="image/x-icon" />
 


    <!--    <link rel="apple-touch-icon" href="/assets/backend/ico/apple-touch-icon.png" />-->
    <!--    <link rel="apple-touch-icon" sizes="57x57" href="/assets/backend/ico/apple-touch-icon.png" />-->
    <!--    <link rel="apple-touch-icon" sizes="72x72" href="/assets/backend/ico/apple-touch-icon.png" />-->
    <!--    <link rel="apple-touch-icon" sizes="76x76" href="/assets/backend/ico/apple-touch-icon.png" />-->
    <!--    <link rel="apple-touch-icon" sizes="114x114" href="/assets/backend/ico/apple-touch-icon.png" />-->
    <!--    <link rel="apple-touch-icon" sizes="120x120" href="/assets/backend/ico/apple-touch-icon.png" />-->
    <!--    <link rel="apple-touch-icon" sizes="144x144" href="/assets/backend/ico/apple-touch-icon.png" />-->
    <!--    <link rel="apple-touch-icon" sizes="152x152" href="/assets/backend/ico/apple-touch-icon.png" />-->
    <!--    <link rel="apple-touch-icon" sizes="180x180" href="/assets/backend/ico/apple-touch-icon.png" />-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/assets/js/html5shiv.js"></script>
    <script src="/assets/js/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    @include('admin.partials.nav')
    <div class="wrapper">
        @yield('main')
    </div>




<!-- javascript -->
<script src="<?php if($edit == "edit"){echo "../";} ?><?php if($segment != ""){echo "../";} ?>../../public/assets/js/jquery.js"></script>
<script src="<?php if($edit == "edit"){echo "../";} ?><?php if($segment != ""){echo "../";} ?>../../public/assets/js/bootstrap.min.js"></script>
<script src="<?php if($edit == "edit"){echo "../";} ?><?php if($segment != ""){echo "../";} ?>../../public/assets/js/ckeditor/ckeditor.js"></script>
<script src="<?php if($edit == "edit"){echo "../";} ?><?php if($segment != ""){echo "../";} ?>../../public/assets/js/ckeditor/adapters/jquery.js"></script>
<script src="<?php if($edit == "edit"){echo "../";} ?><?php if($segment != ""){echo "../";} ?>../../public/assets/js/bootstrap-fileupload.min.js"></script>
<script src="<?php if($edit == "edit"){echo "../";} ?><?php if($segment != ""){echo "../";} ?>../../public/assets/js/main.js"></script>

<script src="<?php if($edit == "edit"){echo "../";} ?><?php if($segment != ""){echo "../";} ?>../../node_modules/bootbox/bootbox.min.js"></script>
    <script>
        $(document).on("click", ".alert", function(e) {
            bootbox.confirm("Are you sure?", function(result) {
                Example.show("Confirm result: "+result);
            }); 
        });
    </script>

<script type="text/javascript">   
$(document).ready(function(){
    $("#selecctall").change(function(){
      $(".checkbox1").prop('checked', $(this).prop("checked"));
      });
});</script>

@yield('styles')
@yield('scripts')

</body>
</html>


@extends('admin.layouts.default')
{{-- Content --}}
@section('main')

<div class="n_titlepage ptd20">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <h2 class="str s21 mb0">Add New {!! trans("admin/bank.bank"); !!}</h2>
            </div>
            <div class="col-md-3 col-sm-4"></div>
        </div>
    </div>
</div>

@if (isset($bank))
    {!! Form::model($bank, array('url' => URL::to('admin/bank') . '/' . $bank->id, 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
@else
    {!! Form::open(array('url' => URL::to('admin/bank'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
@endif

<div class="ptd60">
    <div class="container">
        <div class="n_bx ptd0">
            <div class="row no-gutter">
                <div class="col-md-9 col-sm-8">
                    <div class="p30">
                            <div class="form-group  {{ $errors->has('code') ? 'has-error' : '' }}">
                            {!! Form::label('code', trans("admin/bank.code"), array('class' => 'control-label')) !!}
                            <div class="controls">
                            {!! Form::text('code',(isset($bank)? $bank->code : ''), array('class' => 'form-control')) !!}
                            <span class="help-block">{{ $errors->first('code', ':message') }}</span>
                            </div>
                            </div>
                            
                            <div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
                            {!! Form::label('name', trans("admin/bank.name"), array('class' => 'control-label')) !!}
                            <div class="controls">
                            {!! Form::text('name', (isset($bank) ? $bank->name : ''), array('class' => 'form-control')) !!}
                            <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                            </div>
                            </div>

                            
                            <div class="form-group  {{ $errors->has('headquarter') ? 'has-error' : '' }}">
                            {!! Form::label('headquarter', trans("admin/bank.headquarter"), array('class' => 'control-label')) !!}
                            <div class="controls">
                            {!! Form::text('headquarter', (isset($bank) ? $bank->headquarter : ''), array('class' => 'form-control')) !!}
                            <span class="help-block">{{ $errors->first('headquarter', ':message') }}</span>
                            </div>
                            </div>
                        
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 bg_grey b_left">
                    <div class="p30 b_bottom">
                        <h3 class="str">Publish</h3>
                        <ul class="list-none list-inline tar">
                            <li><a href="../user" class="btn btn-transparent">Cancel</a></li>
                            <li><button type="submit" class="btn btn-blue str">Save</button></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    {!! Form::close() !!}
    @stop @section('scripts')
        <script type="text/javascript">
            $(function () {
                $("#roles").select2()
            });
        </script>
</div>
@stop
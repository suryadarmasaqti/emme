@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! trans("admin/bank.bank") !!} :: @parent
@stop

{{-- Content --}}
@section('main')
<form action="bank/action" method="post" class="form form-inline"><input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
<div class="n_titlepage ptd20">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <h2 class="str s21 mb0">{!! trans("admin/bank.bank") !!}</h2>
            </div>
            <div class="col-sm-3 tar">
                <div class="pull-right">
                    <a href="{!! URL::to('admin/bank/create') !!}"
                       class="btn btn-blue str"><span
                                class="glyphicon glyphicon-plus-sign"></span> {{
                    trans("admin/modal.new") }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ptd60">
<div class="container">
<div class="n_bx ptd0">
<div class="p30">

<ul class="list-none list-inline n_filter-table">
                    <li>
                         <div class="form form-inline">
                            <div class="form-group select">
                                <select name="bulkaction" class="form-control">
                                    <option >Bulk Action</option>
                                    <option value="Delete">Delete</option>
                                </select>
                                <i class="fa fa-angle-down"></i>
                            </div>
                            <div class="form-group">
                                <button name="button" value="bulkaction" type="submit"class="btn btn-grey" onclick="return confirm('are you sure you can perform these actions?')" >Apply</button>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="form form-inline">
                            <div class="form-group">
                                <input name="search" type="text" placeholder="Search..." class="form-control">
                            </div>
                            <div class="form-group">
                                <button name="button" value="search" type="submit" class="btn btn-grey">Search</button>
                            </div>
                        </div>
                    </li>
                </ul>

<table class="table n_custom">
                    
                    <thead>
                        <tr>
                            <th class="check"><input type="checkbox" id="selecctall" name="checkAll"></th>
                            <th class="title">{!! trans("admin/bank.name") !!}</th>
                            <th>{!! trans("admin/bank.code") !!}</th>
                            <th>{!! trans("admin/bank.headquarter") !!}</th>
                            <th class="last-edit">{!! trans("admin/admin.created_at") !!}</th>
                            <th class="action">{!! trans("admin/admin.action") !!}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <?php foreach($bank as $value) {?>
                            <td class="check"><input class="checkbox1" type="checkbox" name="check<?php echo "$value->id"; ?>" value="<?php echo "$value->id";?>";></td>
                            <td class="title"><a href="bank/<?php echo "$value->id";  ?>/edit">{{$value->name}}</a></td>
                            <td>{{$value->code}}</td>
                            <td>{{$value->headquarter}}</td>
                            <td class="last-edit">{{$value->created_at}}</td>
                            <td class="action"><a href="bank/<?php echo $value->id ?>/delete" class="btn btn-delete" title="Delete" ><i class="fa fa-trash"></i> Delete</a>
                            </td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td class="check"></td>
                            <td class="title"><b><?php echo (count($bank)) ?> items</b></td>
                            <td class="last-edit"><?php echo str_replace('/?', '?', $bank->render()); ?></td>
                        </tr>
                    </tbody>
                </table>
@stop

{{-- Scripts --}}
@section('scripts')
@stop

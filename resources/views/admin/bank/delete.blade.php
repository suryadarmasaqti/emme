@extends('admin.layouts.modal')
@section('content')
	{!! Form::model($bank, array('url' => URL::to('admin/bank') . '/' . $bank->id, 'method' => 'delete', 'class' => 'bf', 'files'=> true)) !!}
					<a href="{{ URL::to('admin/bank') }}" class="btn btn-default" data-dismiss="modal">Cancel</a>
                    <button type="submit" class="btn btn-blue">Delete</button>
	{!! Form::close() !!}
@stop
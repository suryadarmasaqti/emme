
@extends('layouts.login')
@section('content')
{!! Form::open(array('url' => URL::to('auth/login'), 'method' => 'post', 'files'=> true)) !!}
    <div class="table-dis">
        <div class="table-cell-dis tac">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                        <img src="../assets/img/icon/clementi-icn-fav.png" class="mb10">
                        <h1>Sign in to <b>clementi</b></h1>
                        <div class="n_bx n_login">
                            <div class="form tal" method="post" action = 'proseslogin'><input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                <div class="form-group {{$errors->has('email') ? 'n_error-field' : ''}}">
                                    <label class="str s16 " >Email Address</label>
                                    <input name="email" value="<?php echo Input::old('email')?>" type="text" class="form-control" placeholder="Enter Email Addres...">
                                    <div class="n_error">
                                         {{$errors->has('email') ? $errors->first('email') : ''}}
                                    </div>
                                </div>
                                <div class="form-group {{$errors->has('password') ? 'n_error-field' : ''}}">
                                    <div class="clearfix">
                                        <div class="pull-left">
                                            <label class="str s16">Password</label>
                                        </div>
                                    </div>
                                    <input name="password" type="password" class="form-control" placeholder="Enter password...">
                                        <div class="n_error">
                                             {{$errors->has('password') ? $errors->first('password') : ''}}
                                        </div>
                                    </div>
                                <button class="btn btn-blue btn-block str s16" onclick="return checkmail(this.form.mail)" >Sign In</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection





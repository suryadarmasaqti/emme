
<header class="header">
    <div class="clearfix">
        <div class="pull-left n_comp">
            <div class="n_nav-xs hidden-lg"><a href="#"><i class="fa fa-bars s24"></i></a></div>
            <ul class="list-none list-inline">
                <li><h1 class="mb0 str s24"><a href="#">Clm</a></h1></li>
                <li><a href="#" class="btn btn-white small s12">Visit site</a></li>
            </ul>
        </div>
        <div class="pull-left n_nav">
            <nav>
               <ul class="list-none list-inline str">
                    <?php $segment = Request::segment(1);?>
                    <li class=<?php if($segment==""){echo "active";}?> ><a href='dashboard'>Dashboard</a></li>
                    <li><a href="all-page">Pages</a></li>
                    <li class="dropdown ">
                        <a href="#" data-toggle="dropdown">Posts</a>
                        <ul class="dropdown-menu n_custom">
                            <li><a href="all-post">All Posts</a></li>
                            <li><a href="add-post">Add New Post</a></li>
                            <li><a href="all-category">All Category</a></li>
                            <li><a href="add-category">Add New Category</a></li>
                        </ul>
                    </li>
                    <li><a href="gallery">Gallery</a></li>
                    <li class=<?php if($segment=="user"){echo "active";}?>><a href="admin/user">Users</a></li>
                    <li><a href="setting?sett=all">Settings</a></li>
                </ul>
            </nav>
        </div>
        <div class="pull-right n_users">
            <div class="dropdown">
                <a href="#" data-toggle="dropdown">Welcome Username<i class="fa fa-bars"></i></a>
                <ul class="dropdown-menu n_custom n_right">
                    <li><a href="profile">Profile</a></li>
                    <li><a href="setting?sett=all">Settings</a></li>
                    <li><a href="auth/logout">Logout</a></li>
                </ul>
            </div>
        </div>
    </div>

</header>


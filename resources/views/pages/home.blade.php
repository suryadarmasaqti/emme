
@extends('layouts.master')
@section('content')
    @include('partials.titlepage')
    <div class="ptd60">
    <div class="container">
        <div class="n_bx">
            <div class="clearfix mb30">
                <div class="pull-left">
                    <h2 class="s21 str mb0">Welcome to Clementi CMS!</h2>
                    <div class="s16">We’ve assembled some links to get you started:</div>
                </div>
                <div class="pull-right hidden-xs">
                <?php if (isset($_GET['hiden'])) {?> <a href="../public">Show</a><?php }else{?> <a href="?hiden=hide">Hide</a> <?php } ?>
                </div>
            </div>
            <div class="row" <?php if (isset($_GET['hiden'])) { echo "hidden"; } ?> >
                <div class="col-lg-6 col-md-8">
                    <ul class="list-none list-inline n_action">
                        <li><a href="#"><span><img src="assets/img/icon/clementi-icn-edit.png"></span><span>Edit your front page</span></a></li>
                        <li><a href="add-post"><span><img src="assets/img/icon/clementi-icn-add-post.png"></span><span>Add new post</span></a></li>
                        <li><a href="add-page"><span><img src="assets/img/icon/clementi-icn-add-page.png"></span><span>Add new page</span></a></li>
                        <li><a href="gallery"><span><img src="assets/img/icon/clementi-icn-add-gallery.png"></span><span>Add new gallery</span></a></li>
                        <li><a href="#"><span><img src="assets/img/icon/clementi-icn-view-site.png"></span><span>View your site</span></a></li>
                        <li><a href="register"><span><img src="assets/img/icon/clementi-icn-add-user.png"></span><span>Add new user</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!-- @extends('layouts.app')
@section('title') Home :: @parent @stop
@section('content')
<div class="row">
    <div class="page-header">
        <h2>Home Page</h2>
    </div></div>

    @if(count($articles)>0)
        <div class="row">
            <h2>News</h2>
            @foreach ($articles as $post)
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-8">
                            <h4>
                                <strong><a href="{{URL::to('article/'.$post->slug.'')}}">{{
                                        $post->title }}</a></strong>
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <a href="{{URL::to('news/'.$post->slug.'')}}" class="thumbnail"><img
                                        src="http://placehold.it/260x180" alt=""></a>
                        </div>
                        <div class="col-md-10">
                            <p>{!! $post->introduction !!}</p>

                            <p>
                                <a class="btn btn-mini btn-default"
                                   href="{{URL::to('news/'.$post->slug.'')}}">Read more</a>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p></p>

                            <p>
                                <span class="glyphicon glyphicon-user"></span> by <span
                                        class="muted">{{ $post->author->name }}</span> | <span
                                        class="glyphicon glyphicon-calendar"></span> {{ $post->created_at }}
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif

    @if(count($photoAlbums)>0)
        <div class="row">
            <h2>Photos</h2>
            @foreach($photoAlbums as $item)
                <div class="col-sm-3">
                    <div class="row">
                        <a href="{{URL::to('photo/'.$item->id.'')}}"
                           class="hover-effect">
                            @if($item->album_image!="")
                                <img class="col-sm-12"
                                        src="{!! URL::to('appfiles/photoalbum/'.$item->folder_id.'/'.$item->album_image) !!}">
                            @elseif($item->album_image_first!="")
                                <img class="col-sm-12"
                                     src="{!! URL::to('appfiles/photoalbum/'.$item->folder_id.'/'.$item->album_image_first) !!}">
                            @else
                                <img class="col-sm-12" src="{!! URL::to('appfiles/photoalbum/no_photo.png') !!}">
                            @endif
                        </a>
                        <div class=" col-sm-12">{{$item->name}}</div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif

@endsection

 -->
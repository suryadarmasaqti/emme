<!doctype html>
<html>
<head>
    <title>Login - Clementi 2.2</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta name="author" content="(c) 2014" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="../style.css" rel="stylesheet" media="screen">

    <link rel="shortcut icon" href="..assets/img/icon/clementi-icn-fav.png" type="image/x-icon" />
    <!--    <link rel="apple-touch-icon" href="/assets/backend/ico/apple-touch-icon.png" />-->
    <!--    <link rel="apple-touch-icon" sizes="57x57" href="/assets/backend/ico/apple-touch-icon.png" />-->
    <!--    <link rel="apple-touch-icon" sizes="72x72" href="/assets/backend/ico/apple-touch-icon.png" />-->
    <!--    <link rel="apple-touch-icon" sizes="76x76" href="/assets/backend/ico/apple-touch-icon.png" />-->
    <!--    <link rel="apple-touch-icon" sizes="114x114" href="/assets/backend/ico/apple-touch-icon.png" />-->
    <!--    <link rel="apple-touch-icon" sizes="120x120" href="/assets/backend/ico/apple-touch-icon.png" />-->
    <!--    <link rel="apple-touch-icon" sizes="144x144" href="/assets/backend/ico/apple-touch-icon.png" />-->
    <!--    <link rel="apple-touch-icon" sizes="152x152" href="/assets/backend/ico/apple-touch-icon.png" />-->
    <!--    <link rel="apple-touch-icon" sizes="180x180" href="/assets/backend/ico/apple-touch-icon.png" />-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/assets/js/html5shiv.js"></script>
    <script src="/assets/js/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div class="wrapper">
    @yield('content')
</div>
@yield('styles')

<!-- javascript -->
<script src="../assets/js/jquery.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/main.js"></script>

</body>
</html>